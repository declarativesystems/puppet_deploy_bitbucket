# Puppet Deploy Plugin for Bitbucket Server

## What's this?
A plugin to trigger a refresh of your 
[Puppet Control Repository](https://puppet.com/docs/pe/2019.1/control_repo.html#setting-up-a-control-repository)
on your Puppet Enterprise Master when code is pushed.

## How do I use this?
Please see the plugin page on
[Atlassian Marketplace](https://marketplace.atlassian.com/apps/1217802/puppet-deploy-for-bitbucket-server?hosting=server&tab=overview)

## Which plugin version do I use?
All new installations should use the latest plugin. This requires BitBucket 
Server 5.2.0 or greater.

There are several versions of the plugin to support older Bitbucket Servers. 
Since these older versions of Bitbucket are now EOL, the following applies for
reference only:

| Bitbucket Server | Plugin version             | Notes                                   |
| ---              | ---                        | ---                                     |
| 5.0.0 - 5.1.7    | Denoted `-5`, eg `1.4.0-5` | Bitbucket 5.1 is EOL since June 2019    |
| 4x               | Denoted `-4`, eg `1.4.0-4` | Bitbucket 4x is EOL since February 2019 |
