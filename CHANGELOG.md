# 2.0.0

## Features
* Support waiting for deployment to complete so that final deployment status can
  be reported
* Support for skipping deployment for named environments
* Check that puppet's revision ID matches the one in Bitbucket on `git push`
* UI improvements

## Bugs
* Fixed error where chrome wrongly clobbers `Puppet Master FQDN` and 
  `RBAC Token` with the bitbucket username and and password
* Fixed handling of pushing several branches at once
