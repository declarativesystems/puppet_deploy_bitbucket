---
title: Usage
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bitbucket
---

## git push
Once enabled, every time someone does `git push` to the repository, the hook 
will deploy all of the branches within the push to Puppet Enterprise excluding
any that are in the `Skip Environments` list.

The git client prints deployment results as part of the `git push` output:

![deploy ok](../images/push_ok.png)
_With `wait` checked in settings, we verify Puppet Enterprise deployed the
correct code revision_

![deploy ok](../images/push_queued.png)
_With `wait` unchecked, we only report if Puppet Enterprise accepted our
deployment job_

## Deploy code whenever you like
You can return to the plugin settings page any time you like by clicking
the pencil icon to edit settings ![edit settings](../images/plugin_edit.png)

From the settings screen, use the radio buttons under `Actions` to deploy
either _All environments_ or a _Specific environment_ using the branch
selector, then click `Deploy`. 

The plugin displays the results from Puppet:

![deploy ok](../images/deploy_production_ok.png)

## Deployment errors
Code Manager can fail mid-deployment for reasons such as: 

* Wrong git repository details/credentials
* Errors in `Puppetfile`
* Trouble reaching Puppet Forge
* ...etc

We capture and report the deployment status from Puppet Enterprise:

![push](../images/deploy_all_env.png)
_Deploying using plugin settings - click `Toggle Code Manager JSON` to view the
error message from Puppet_

![push](../images/push_error_detail.png)
_Deploying from `git push` (`wait` must be checked). The error message from Puppet is
always printed when errors are found_

In these examples the errors were:
* `fail_code_quality` environment wasn't deployed because `Puppetfile` contains 
  errors
* `development` branch reports `MISMATCH` between the current revision in 
  Bitbucket (git) vs Puppet. This was caused by connecting Puppet to a
  completely different git server that has a branch of the same name. This kind
  of error is only detected during `git push`.
