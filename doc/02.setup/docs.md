---
title: Setup
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bitbucket
---

## Puppet Enterprise
1. [Configure Code Manager](https://puppet.com/docs/pe/2019.1/code_mgr_config.html#configuring-code-manager)
   to connect to your Bitbucket repository 
2. Generate a token using:
    * [Manual instructions](https://puppet.com/docs/pe/2019.1/rbac_token_auth_intro.html)
    * [pe_rbac](https://github.com/declarativesystems/pe_rbac)
    * The token can be found at `~/.puppetlabs/token`
3. Obtain the CA Certificate (optional)
    * Copy the contents of `/etc/puppetlabs/puppet/ssl/ca/ca_crt.pem` from the
      Puppet Master

## Plugin
1. Install the plugin (follow the Atlassian Marketplace instructions)
2. Navigate to your
   [Puppet Control repository](https://puppet.com/docs/pe/2019.1/control_repo.html#setting-up-a-control-repository)
   in Bitbucket
3. Click `Settings`(cog)

   ![settings](../images/repo_setup.png)   
4. Click `Hooks` and enable _Puppet Deploy_ 

   ![hooks](../images/enable_hook.png)
5. Enter the details you collected on the settings page:

   ![setup](../images/plugin_setup.png)
    1. `Puppet Master FQDN`
    2. `RBAC Token`
    3. `CA Certificate` (optional)
    4. `Skip Environments` (optional)
        * Space delimited list of environments that will not be deployed
          automatically on `git push`, eg `production`
    5. `Wait`
        * _checked_ - Wait for Puppet Code Manager to return deployment status during 
          `git push`
        * _unchecked_ - Stop once Puppet accepts deployment request
6. Click `Deploy` to do a test deployment
7. Click `Enable` to save changes and enable the hook
