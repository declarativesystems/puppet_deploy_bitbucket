---
title: Overview
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bitbucket
---
# Puppet Deploy Plugin for Bitbucket Server
Deploy Puppet code from Bitbucket Server to Puppet Enterprise:

* Post-receive hook triggers code manager automatically
* Plugin settings page lets you deploy code whenever you like

![montage](../images/puppet_deploy_bitbucket_montage.png)


## Features
* Simple configuration though the plugin settings page
* Test and diagnose deployment problems
* Optional SSL certificate validation without having to alter the Java trust
  store
* Skip automatic deployment of named branches
* Choose to wait for confirmation of deployment or return immediately
* Deployment status printed on every git push
