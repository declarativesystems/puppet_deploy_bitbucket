/*
 * Copyright 2017 Declarative Systems PTY LTD
 * Copyright 2016 Puppet Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.declarativesystems.bitbucket.puppetdeploy.hook;

import com.atlassian.bitbucket.hook.repository.PostRepositoryHook;
import com.atlassian.bitbucket.hook.repository.PostRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookRequest;
import com.atlassian.bitbucket.hook.repository.SynchronousPreferred;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.declarativesystems.bitbucket.puppetdeploy.servlet.ConfigServlet;
import com.declarativesystems.pejava.codemanager.Deploy;
import com.declarativesystems.pejava.codemanager.DeployResult;
import com.google.gson.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Named
@SynchronousPreferred
public class PuppetDeployHook implements PostRepositoryHook, SettingsValidator {
    private static final Logger log = LoggerFactory.getLogger(PuppetDeployHook.class);
    public static final String PUPPET_MASTER_FQDN = "puppetMasterFqdn";
    public static final String TOKEN = "token";
    public static final String CA_CERT = "caCert";
    public static final String WAIT = "wait";
    public static final String SKIP_ENVIRONMENTS = "skipEnvironments";
    private static final String OUTPUT_INDENT = "\t";

    private final PluginLicenseManager pluginLicenseManager;

    private final Deploy deploy;


    @Inject
    public PuppetDeployHook(
            @ComponentImport  PluginLicenseManager pluginLicenseManager,
            final Deploy deploy) {
        this.pluginLicenseManager = pluginLicenseManager;
        this.deploy = deploy;
    }


    @Override
    public void validate(@Nonnull Settings settings,@Nonnull SettingsValidationErrors errors,@Nonnull Scope scope) {
        String puppetMasterFqdn = settings.getString(PUPPET_MASTER_FQDN, "");
        if (StringUtils.isBlank(puppetMasterFqdn)) {
            errors.addFieldError(PUPPET_MASTER_FQDN, "Puppet Master FQDN field is blank, please supply one");
        } else {
            // validating hostnames/IP addresses is HARD these days, so just attempt to lookup
            // an IP address and report failure if encountered
            try {
                InetAddress.getByName(puppetMasterFqdn);
            } catch (UnknownHostException e) {
                errors.addFieldError(PUPPET_MASTER_FQDN, "Unable to resolve Puppet Master FQDN to an IP address:  " + e.getMessage());
            }
        }


        if (StringUtils.isBlank(settings.getString(TOKEN, ""))) {
            errors.addFieldError(TOKEN, "Token field is blank, please supply one");
        }
    }

    @Override
    public void postUpdate(@Nonnull PostRepositoryHookContext postRepositoryHookContext, @Nonnull RepositoryHookRequest repositoryHookRequest) {
        Settings settings = postRepositoryHookContext.getSettings();
        String puppetMasterFqdn = settings.getString(PUPPET_MASTER_FQDN, "");
        String token = settings.getString(TOKEN, "");
        String caCert = settings.getString(CA_CERT, "");
        String[] skipEnvironments = settings.getString(SKIP_ENVIRONMENTS, "").split(" ");
        boolean wait = settings.getBoolean(WAIT, false);
        String tag = "Puppet Deploy Plugin:  ";

        // just use the `out` stream - stuff on error works but doesn't
        // seem differentiated on the client and comes out in the wrong order too
        PrintWriter out = repositoryHookRequest.getScmHookDetails().orElseThrow(IllegalStateException::new).out();

        if (StringUtils.isEmpty(puppetMasterFqdn) || StringUtils.isEmpty(token)) {
            String message = String.format("%s Not setup, please configure the plugin (skipped)", tag);
            log.error(message);
            out.println(message);
        } else {
            Map<String, String> target = new HashMap<>();
            for (RefChange refChange : repositoryHookRequest.getRefChanges()) {
                // its possible to deploy many environments at once (eg git push --all)
                // so we need to collect up the commits we want for each environment
                // to check them all after the deployment
                String environment = refChange.getRef().getDisplayId();
                if (ArrayUtils.contains(skipEnvironments, environment)) {
                    out.println(String.format(
                            "Skipping environment: %s as requested",
                            environment
                    ));
                } else {
                    target.put(
                            environment,
                            refChange.getToHash()
                    );
                }
            }

            if (! target.isEmpty()) {
                try {
                    String result = deploy.deployCode(
                            puppetMasterFqdn,
                            token,
                            caCert,
                            target.keySet().toArray(new String[0]),
                            wait
                    );

                    boolean errors = DeployResult.responseStringContainError(result);

                    // build a coherent deployment resport so it can be flushed
                    // to the logs in one go
                    StringBuilder deploymentReport = new StringBuilder();
                    deploymentReport.append(
                            "Deployment results for Puppet Server ")
                            .append(puppetMasterFqdn)
                            .append(":\n");

                    // each deployment result
                    for (DeployResult deployResult : DeployResult.checkDeployResult(result, target)) {
                        deploymentReport.append(OUTPUT_INDENT)
                                .append(deployResult.toString())
                                .append("\n");
                    }

                    if (errors) {
                        deploymentReport.append(
                                "Error reported by Puppet: \n")
                                .append(DeployResult.prettyPrintJson(result));
                    }

                    out.print(deploymentReport.toString());
                    // have to select the right log...
                    // https://jira.qos.ch/browse/SLF4J-124
                    if (errors) {
                        log.error(deploymentReport.toString());
                    } else {
                        log.info(deploymentReport.toString());
                    }

                } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
                    String message = String.format("%s Error deploying environment %s on %s, cause: %s",
                            tag,
                            String.join(",", target.keySet()),
                            puppetMasterFqdn,
                            e.getMessage()
                    );
                    log.error(message, e);
                    out.println(message);
                }
            }
        }
    }
}
